<?php
/*StripeService.php
Symfony Service to connect to Cloudinary
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



namespace CYINT\ComponentsPHP\Services;
use Doctrine\Bundle\DoctrineBundle\Registry;
use \Stripe\Stripe as Stripe;
use \Stripe\Customer as Customer;
use \Stripe\Charge as Charge;
use \Stripe\Error\Card as CardError;
use \Stripe\Plan as Plan;
use \Stripe\Subscription as Subscription;
use \Stripe\Event as Event;
use \Stripe\Transfer as Transfer;
use CYINT\ComponentsPHP\Services\CurlService;

class StripeService
{
    protected $public_key = null;
    protected $private_key = null;
    protected $client_id = null;
    protected $CurlService = null;
    protected $connect_url = null;

    function __construct(Registry $Doctrine, CurlService $CurlService)
    { 
        $Repository = $Doctrine->getRepository('CYINTSettingsBundle:Setting');
        $stripe = $Repository->findByNamespace('stripe');
        $public_key = $stripe['public_key'];
        $private_key = $stripe['private_key'];
        $client_id = empty($stripe['client_id']) ? null : $stripe['client_id'];
        $this->connect_url = 'https://connect.stripe.com/oauth/token'; 
        $this->setPublicKey($public_key);
        $this->setPrivateKey($private_key);
        $this->setClientId($client_id);
        $this->setCurlService($CurlService);            
        $this->setConfiguration();        
    }

    private function setConfiguration()
    {
        Stripe::setApiKey($this->private_key);
    }

    public function getPublicKey()
    {
        return $this->public_key;
    }

    public function setPublicKey($public_key)
    {
        $this->public_key = $public_key;
        $this->setConfiguration();
        return $this;
    }

    public function getPrivateKey()
    {
        return $this->private_key;
    }

    public function setPrivateKey($private_key)
    {
        $this->private_key = $private_key;
        $this->setConfiguration();
        return $this;
    }

    public function setClientId($client_id)
    {
        $this->client_id = $client_id;
    }

    public function getClientId()
    {
        return $this->client_id;
    }

    public function initiateTransaction($email, $token, $amount) 
    {
        try 
        {
            $customer = Customer::create(['email'=>'', 'source'=>$token]);

            $charge = Charge::create([
                'customer'=>$customer->id,
                'amount' => $amount,
                'currency' => 'usd'
            ]);

            if($charge->status != 'succeeded' && $charge->status != 'pending')
                throw new \Excepton($charge->failure_message, 403);

            return ['success'=>true, 'charge'=>$charge];
        } 
        catch(StripeServiceException $e) 
        {
            return ['success'=>false, 'error'=>$e];
        }
    }

    public function createSubscriptionPlan($name, $id, $interval, $currency, $amount)
    {
		$plan = Plan::create(array(
		  "name" => $name
		  ,"id" => $id
		  ,"interval" => $interval
		  ,"currency" => $currency
		  ,"amount" => $amount
		));
        
        return $plan;
    }

    public function createCustomer($email, $token = null)
    {
        try
        {
            $options = [];
            $options['email'] = $email;
            if(!empty($token))
                $options['source'] = $token;

            $customer = Customer::create($options);
        }
        catch(StripeServiceException $Ex)
        {
            return null;        
        }

        return $customer;
    }

    public function subscribeCustomer($customer, $plan, $coupon)
    {
       
        try 
        {
            $options = [
                'customer' => $customer
                ,'plan' => $plan
            ];

            if(!empty($coupon))
                $options['coupon'] = $coupon;

            $result = Subscription::create($options);

        } 
        catch(StripeServiceException $e) 
        {
            return null;
        }      

        return $result;
    }

    public function retrieveSubscription($id)
    {
        try 
        {
            $result = Subscription::retrieve($id);
        } 
        catch(StripeServiceException $e) 
        {
            return null;
        }      
        return $result;

    }

    public function retrieveCustomer($id)
    {
        try 
        {
            $result = Customer::retrieve($id);
            if(!empty($result['deleted']) && $result['deleted'] == true)
            {
                return null;
            }
        } 
        catch(StripeServiceException $e) 
        {
            return null;
        }    
 
        return $result;
 
    }

    public function retrieveEvent($id)
    {
        $result = Event::retrieve($id);
        return $result;
    }

    public function listAllPlans()
    {
        return Plan::all();        
    }

    public function cancelSubscription($subscription_id)
    {
        $subscription = $this->retrieveSubscription($subscription_id);
        $subscription->cancel();
    }

    public function setCurlService(CurlService $CurlService)
    {
        $this->CurlService = $CurlService;
    }

    public function getCurlService()
    {
        return $this->CurlService;
    }

    public function getStripeAccountFromCode($token_request_body)
    {
        $this->getCurlService()->setPostData(http_build_query($token_request_body));
        $response = json_decode($this->getCurlService()->processUrl($this->connect_url), true);
        if(!empty($response['error']))
            throw new StripeServiceException($response['error_description']);

        return $response;
    }

    public function initiateSourceTransfer($charge_id, $destination_account, $amount)
    {
        $transfer = Transfer::create([
            'amount' => $amount
            ,'currency' => 'usd'
            ,'source_transaction' => $charge_id
            ,'destination' => $destination_account
        ]);
    
         if(empty($transfer->object) || $transfer->object != 'transfer')
            throw new \Excepton($transfer->error->message, 403);

         return ['success'=>true, 'transfer'=>$transfer];
       
    }
}
